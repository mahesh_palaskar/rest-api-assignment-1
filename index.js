const express=require('express')
const bodyParser = require("body-parser")
const customerRouter=require('./routers/customers')
const mongoose=require('mongoose')
require('dotenv');

const url='mongodb://localhost/mydatabase'
const app=express()

app.use(express.json())
//connect to database
mongoose.connect(url,{useNewUrlParser:true})

const conObject=mongoose.connection

conObject.on('open',()=>{
      console.log('connected...')
})
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/customers',customerRouter)

const port=process.env.PORT || 5000;


app.listen(port,'localhost',()=>
{
      console.log(`server started on port: ${port}`)
})


