
const express=require('express')

const router=express.Router()
const Customer=require('./models/customer')

router.get('/',async(req,res)=>{
  try{
    const customers= await Customer.find()
    res.json(customers)
  }catch(err)
  {
    res.send('Error'+err)
  }
})


router.get('/:id',async(req,res)=>{
  try{
    const customers= await Customer.findById(req.params.id)
    res.json(customers)
  }catch(err)
  {
    res.send('Error'+err)
  }
})

router.post('/',async(req,res)=>{
  const customer=new Customer({
    name:req.body.firstName,
    age:req.body.age
  })
  try{
    const c1=await customer.save()
    res.json(c1)
  }catch(err)
  {
   res.send('Error'+err)
  }
})

module.exports=router